@foreach($credit_cards as $card)
    <tr>
        <td class="card" colspan="2">
            <div class="">
                <div class="checkout__radio">
                    <input type="radio" name="checkout__check" id="card"
                           class="checkout__checkbox"/>
                    <label class="checkout__label" for="card">
                        <img src="{{$card->imageUrl}}" alt="" class=""/>
                        <span>&nbsp;</span>
                    </label>
                    <span class="text-center">************{{$card->last4}}
                        <span>
                            <span>&nbsp;</span>
                        </span>
                    </span>
                </div>
            </div>
        </td>
    </tr>
@endforeach