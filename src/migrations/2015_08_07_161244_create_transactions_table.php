<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->char('transaction_id',10);
			$table->char('processorAuthorizationCode',10)->nullable();
			$table->string('status')->nullable();
			$table->string('transaction_type',15)->nullable();
			$table->string('cardType',50)->nullable();
			$table->string('customerLocation',15);
			$table->text('creditCard_imageUrl');
			$table->string('last4_card',4);
			$table->string('currencyIsoCode',10);
			$table->decimal('amount',5,2);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
