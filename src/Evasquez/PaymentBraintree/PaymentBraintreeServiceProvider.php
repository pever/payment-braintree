<?php namespace Evasquez\PaymentBraintree;

use Illuminate\Support\ServiceProvider;
use Evasquez\PaymentBraintree\Customer\Repo\CustomerRepository;
use Evasquez\PaymentBraintree\PaymentMethod\Repo\PaymentMethodRepository;
use Evasquez\PaymentBraintree\Transaction\Repo\TransactionRepository;
use Evasquez\PaymentBraintree\Transaction\Repo\Transaction;
use Braintree_Configuration;

/**
 * Class PaymentBraintreeServiceProvider
 * @package Evasquez\PaymentBraintree
 */
class PaymentBraintreeServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
//		$this->package('evasquez/payment-braintree');

        include __DIR__ . '/routes.php';

        Braintree_Configuration::environment(
            $this->app['config']->get('payment.braintree.environment')
        );

        Braintree_Configuration::merchantId(
            $this->app['config']->get('payment.braintree.merchantId')
        );
        Braintree_Configuration::publicKey(
            $this->app['config']->get('payment.braintree.publicKey')
        );
        Braintree_Configuration::privateKey(
            $this->app['config']->get('payment.braintree.privateKey')
        );

        $encryptionKey = $this->app['config']->get('payment.braintree.clientSideEncryptionKey');

        // Register blade compiler for the Stripe publishable key.
        $blade = $this->app['view']->getEngineResolver()->resolve('blade')->getCompiler();
        $blade->extend(function ($value, $compiler) use ($encryptionKey) {
            $matcher = "/(?<!\w)(\s*)@braintreeClientSideEncryptionKey/";
            return preg_replace($matcher, $encryptionKey, $value);
        });

        $this->showPublishedConfig();
	}

    /**
     *
     */
    public function showPublishedConfig(){
        $this->publishes([
            __DIR__.'/config/payment.php' => config_path('payment.php'),
        ], 'payment');
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->registerCustomer();
        $this->registerPaymentMethod();
        $this->registerTransaction();
	}
    
    /**
     * Register User binding
     */
    public function registerCustomer()
    {
        $app = $this->app;
        $app->bind('Evasquez\PaymentBraintree\Customer\Repo\CustomerInterface', function ($app) {
            return new CustomerRepository();
        });
    }

    /**
     *
     */
    public function registerPaymentMethod()
    {
        $app = $this->app;
        $app->bind('Evasquez\PaymentBraintree\PaymentMethod\Repo\PaymentMethodInterface', function ($app) {
            return new PaymentMethodRepository($app['events']);
        });
    }

    /**
     *
     */
    public function registerTransaction()
    {
        $app = $this->app;
        $app->bind('Evasquez\PaymentBraintree\Transaction\Repo\TransactionInterface', function ($app) {
            return new TransactionRepository(new Transaction(),$app['events']);
        });
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
