<?php namespace Evasquez\PaymentBraintree\Controllers;
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

use Evasquez\PaymentBraintree\Customer\Repo\CustomerInterface;
use Request,Config,View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CustomersController
 * @package Evasquez\PaymentBraintree\Controllers
 */
class CustomersController extends \BaseController{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $customerRepo;
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    private $user;

    /**
     * @param CustomerInterface $customerRepo
     */
    function __construct(CustomerInterface $customerRepo)
    {
        $this->customerRepo = $customerRepo;
        $this->user = \Sentry::getUser();

    }

    /**
     * @return mixed
     */
    public function findCustomer(){

        if(Request::ajax())
        {
            $credit_cards = $this->customerRepo->findCustomer($this->user->id)->creditCards;
            $view = View::make(Config::get('payment-braintree::payment.view.payment_method_show'),compact('credit_cards'))->render();
            return $view;
        }
        throw new NotFoundHttpException("page not found",null,404);
    }
}