<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Controllers;

use Evasquez\PaymentBraintree\Transaction\Repo\TransactionInterface;
use Evasquez\PaymentBraintree\Transaction\Repo\TransactionTrait;
use Mychef\CouponDetail\Repo\CouponDetailInterface;
use Input, Response;

/**
 * Class TransactionsController
 * @package Evasquez\PaymentBraintree\Controllers
 */
class TransactionsController extends \BaseController
{

    use TransactionTrait;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var TransactionInterface
     */
    private $transactionRepo;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var \Cartalyst\Sentry\Users\UserInterface
     */
    private $user;
    
    protected $couponDetail;

    /**
     * TransactionsController constructor.
     * @param $transactionRepo
     */
    public function __construct(
        TransactionInterface $transactionRepo,
        CouponDetailInterface $couponDetail
    ){
        $this->transactionRepo = $transactionRepo;
        $this->couponDetail = $couponDetail;
        $this->user = \Sentry::getUser();
    }

    /**
     * @return mixed
     */
    public function sale()
    {
        $discount = 0; $coupon = Input::get('coupon');
        if ($coupon != '') {
            $coupon = $this->couponDetail->findByUserAndCode($this->user->id, $coupon)->first();
            if(!isset($coupon)) {
                throw new \Exception("The coupon entered is not valid");
            }
            $discount = $coupon->coupon->value;
        }
        $attributes = array(
            'amount' => Input::get('amount') - $discount,
            'paymentMethodNonce' => Input::get('nonce'),
//            'options' => [
//                'submitForSettlement' => True
//            ]
        );

        $result = $this->transactionRepo->sale($attributes);

        if ($result->success) {

            $model = $this->store($this->transactionParseInfo($result));

            return Response::json(array(
                'success' => true,
                'transaction' => $result
            ));
        }

        return Response::json(array(
                'success' => false,
                'message' => $result->message
            )
        );

    }

    /**
     * @param $attributes
     * @return mixed
     */
    private function store($attributes)
    {
        return $this->transactionRepo->save($attributes);
    }

    public function submitForSettlement()
    {
        $transaction_id = Input::get('transaction_id');
        return $this->transactionRepo->submitForSettlement($transaction_id);
    }
}