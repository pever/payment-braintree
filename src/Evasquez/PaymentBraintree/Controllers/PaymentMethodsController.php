<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Controllers;

use Evasquez\PaymentBraintree\PaymentMethod\Repo\PaymentMethodInterface;

/**
 * Class PaymentMethodsController
 * @package Evasquez\PaymentBraintree\Controllers
 */
class PaymentMethodsController extends \BaseController{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $methodRepo;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var
     */
    protected $user;

    /**
     * @param PaymentMethodInterface $methodRepo
     */
    function __construct(PaymentMethodInterface $methodRepo)
    {
        $this->methodRepo = $methodRepo;
        $this->user = \Sentry::getUser();
    }

    /**
     * create new payment method
     * @return mixed
     */
    public function createPaymentMethod()
    {
        return $this->methodRepo->createPaymentMethod($this->user->getId(),array(
            'paymentMethodNonce' => \Input::get('payment_method_nonce')
        ));
    }
}