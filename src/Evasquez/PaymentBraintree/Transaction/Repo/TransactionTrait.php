<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Transaction\Repo;
use Braintree_PaymentMethod;
use Braintree_PaymentMethodNonce;
use Exception, Lang;

/**
 * Class TransactionTrait
 * @package Evasquez\PaymentBraintree\Transaction\Repo
 */
trait TransactionTrait
{

    /**
     * parse info for insert table transactions
     * @param $result
     * @return array
     */
    public function transactionParseInfo($result)
    {

        $attributes = array(
            'user_id' => $result->transaction->customer['id'],
            'transaction_id' => $result->transaction->id,
            'processorAuthorizationCode' => $result->transaction->processorAuthorizationCode,
            'status' => $result->transaction->status,
            'transaction_type' => $result->transaction->type,
            'cardType' => $result->transaction->creditCard['cardType'],
            'customerLocation' => $result->transaction->creditCard['customerLocation'],
            'creditCard_imageUrl' => $result->transaction->creditCard['imageUrl'],
            'last4_card' => $result->transaction->creditCard['last4'],
            'amount' => $result->transaction->amount,
            'currencyIsoCode' => $result->transaction->currencyIsoCode
        );

        return $attributes;
    }

    /**
     * @param $cards
     * @param $type
     * @return array
     */
    public function methodPaymentSelect($cards, $type)
    {
        if ($type == 'list') {
            return $this->methodPaymentParseInfo($cards);
        } elseif ($type == 'default') {
            return $this->methodPaymentDefault($cards);
        }
    }

    /**
     * @param $cards
     * @return array
     */
    public function methodPaymentDefault($cards)
    {
        $default = array();
        foreach ($cards as $card) {
            if ($card->default) {
                $default = $this->parseCard($card);
                break;
            }
        }
        return $default;
    }

    /**
     * @param $result
     * @return array
     */
    public function methodPaymentParseInfo($cards)
    {
        $data = array();
        foreach($cards as $card) {
            $data[] = $this->parseCard($card);
        }
        return $data;
    }

    /**
     * @param $card
     * @return array
     */
    public function parseCard($card)
    {
        return array(
            'token' => $card->token,
            'cardType' => $card->cardType,
            'imageUrl' => $card->imageUrl,
            'expirationDate' => $card->expirationDate
        );
    }

    /**
     * @param $token
     * @param $user_id
     * @return mixed
     * @throws Exception
     */
    public function getNonceByToken($token, $user_id)
    {
        $method = Braintree_PaymentMethod::find($token);
        if ($method->customerId == $user_id) {
            $result = Braintree_PaymentMethodNonce::create($token);
            return $result->paymentMethodNonce->nonce;
        } else {
            throw new Exception(Lang::get('account.pm_dont_permissions'));
        }
    }

    /**
     * @param $token
     */
    public function setDefaultByToken($token)
    {
        \Braintree_PaymentMethod::update(
            $token,
            [
                'options' => [
                    'makeDefault' => true
                ]
            ]
        );
    }

    /**
     * @param $user_id
     * @param $nonce
     * @return mixed
     */
    public function createMethodPayment($user_id, $nonce)
    {
        return Braintree_PaymentMethod::create([
            'customerId' => $user_id,
            'paymentMethodNonce' => $nonce,
            'options' => [
                'verifyCard' => true
            ]
        ]);
    }

    /**
     * @param $message
     * @param $user_id
     * @param $amount
     * @return array
     * @throws Exception
     */
    public function verifyMethodPayment($message, $user_id, $amount, $merchant_account_id)
    {
        if ($message == 'Cannot use a payment_method_nonce more than once.') {
            $customer = \Braintree_Customer::find($user_id); $token = '';
            foreach($customer->creditCards as $card) {
                if ($card->default) {
                    $token = $card->token; break;
                }
            }
            if ($token != '') {
                $attributes = array(
                    'amount' => $amount,
                    'paymentMethodToken' => $token,
                    'merchantAccountId' => $merchant_account_id
                );
                $result = \Braintree_Transaction::sale($attributes);
                if ($result->success) {
                    return $result;
                }
            }
        }
        throw new Exception(Lang::get('messages.transaction_fail'));
    }
}