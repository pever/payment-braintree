<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Transaction\Repo;

use Illuminate\Events\Dispatcher;
use Braintree_Transaction;

class TransactionRepository implements TransactionInterface
{
    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    private static $_SUBMITTED_FOR_SETTLEMENT = "submitted_for_settlement";

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var string
     */
    private static $_AUTHORIZED = "authorized";

    /**
     * @var string
     */
    private static $_VOIDED = "Voided";

    /**
     * @var string
     */
    private static $_REFUND = "Refund";

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var Dispatcher
     */
    private $event;

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var Transaction
     */
    private $model;

    /**
     * @param Transaction $model
     * @param Dispatcher $event
     */
    public function __construct(Transaction $model, Dispatcher $event)
    {
        $this->event = $event;
        $this->model = $model;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function sale(array $attributes)
    {
        $result = Braintree_Transaction::sale($attributes);

        $this->event->fire('transaction.sale', array($result));

        //TODO: insert to data base casioli

        return $result;
    }

    /**
     * save to data base
     * @param array $attributes
     * @return mixed
     */
    public function save(array $attributes)
    {
        $model = $this->model->create($attributes);
        return $this->model->find($model->max('id'));

    }

    /**
     * @param $transaction_id
     * @param null $amount
     * @return object
     */
    public function submitForSettlement($transaction_id, $amount = null)
    {
        $transaction = $this->model->find($transaction_id);

        $result = Braintree_Transaction::submitForSettlement($transaction->transaction_id,$amount);

        $attributes['status'] = self::$_SUBMITTED_FOR_SETTLEMENT;

        $this->model->findOrFail($transaction_id)->update($attributes);

        return $result;
    }

    /**
     * @param $transaction_id
     * @return mixed
     */
    public function reject($transaction_id)
    {
        $transaction = $this->model->find($transaction_id);

        $result = Braintree_Transaction::void($transaction->transaction_id);

        $attributes['status'] = self::$_VOIDED;

        $this->model->findOrFail($transaction_id)->update($attributes);

        return $result;
    }

    /**
     * @param $transaction_id
     * @return object
     */
    public function onlyReject($transaction_id)
    {
        $result = Braintree_Transaction::void($transaction_id);
        return $result;
    }

    /**
     * @param $transaction_id
     * @param $amount
     * @return object
     */
    public function refund($transaction_id, $amount)
    {
        $transaction = $this->model->find($transaction_id);

        $result = Braintree_Transaction::refund($transaction->transaction_id,$amount);

        $attributes['status'] = self::$_REFUND;

        $transaction->update($attributes);

        return $result;
    }

    /**
     * @param $transaction_id
     * @return \Illuminate\Support\Collection|static
     */
    public function find($transaction_id)
    {
        return $this->model->findOrFail($transaction_id);
    }

}