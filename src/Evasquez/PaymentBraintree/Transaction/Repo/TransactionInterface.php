<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Transaction\Repo;


/**
 * Interface TransactionInterface
 * @package Evasquez\PaymentBraintree\Transaction\Repo
 */
interface TransactionInterface
{
    /**
     * @param array $attributes
     * @return mixed
     */
    public function sale(array $attributes);

    /**
     * save to data base
     * @param array $attributes
     * @return mixed
     */
    public function save(array $attributes);

    /**
     * @param $transaction_id
     * @return mixed
     */
    public function submitForSettlement($transaction_id);

    /**
     * @param $transaction_id
     * @return mixed
     */
    public function reject($transaction_id);

    /**
     * @param $transaction_id
     * @return mixed
     */
    public function onlyReject($transaction_id);

    /**
     * @param $transaction_id
     * @param $amount
     * @return mixed
     */
    public function refund($transaction_id, $amount);

    /**
     * @param $transaction_id
     * @return mixed
     */
    public function find($transaction_id);
}