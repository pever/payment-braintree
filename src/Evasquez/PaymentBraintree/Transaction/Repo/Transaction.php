<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Transaction\Repo;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('user_id', 'transaction_id', 'processorAuthorizationCode', 'status', 'transaction_date', 'timezone_type', 'timezone', 'transaction_type', 'cardType', 'customerLocation', 'creditCard_imageUrl', 'last4_card', 'amount', 'currencyIsoCode');

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $hidden = array('transaction_id', 'processorAuthorizationCode', 'transaction_type', 'updated_at', 'created_at', 'deleted_at');
}