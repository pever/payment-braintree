<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

return array(

    'braintree' => array(
        'environment' => getenv('BRAINTREE_ENVIRONMENT'),
        'merchantId' => getenv('BRAINTREE_MERCHANTID'),
        'publicKey' => getenv('BRAINTREE_PUBLICKEY'),
        'privateKey' => getenv('BRAINTREE_PRIVATEKEY'),
        'clientSideEncryptionKey' => getenv('BRAINTREE_CLIENTSIDEENCRYPTIONKEY'),
    ),

    'view' => array(
        'payment_method_show' => 'payment-braintree::payment_method.show'
    )
);