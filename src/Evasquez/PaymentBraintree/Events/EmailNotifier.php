<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Events;
use Evasquez\PaymentBraintree\Mailers\SaleMailer;

class EmailNotifier {

    protected $mailer;

    function __construct(SaleMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send a welcome email to the new user
     *
     * @param User $user
     * @return void
     */
    public function handle($user)
    {
        // Send the welcome email
    }

    public function subscribe($events)
    {
        $events->listen('sale.create', 'Evasquez\PaymentBraintree\Events\EmailNotifier@sendMessageSale');
//        $events->listen('users.activate', 'Stevemo\Cpanel\Events\EmailNotifier@sendMessageActivate');
    }

    /**
     * @param $user
     */
    public function sendMessageSale($user)
    {
        $this->mailer->sendConfirmationSale($user);
    }

//    /**
//     * @param $user
//     */
//    public function sendMessageActivate($user)
//    {
//        $this->mailer->sendActivateMessageTo($user);
//    }
}