<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

//PURCHASES

Route::get('/customer/find',
    ['as' => 'customer.find',
        'uses' => 'Evasquez\PaymentBraintree\Controllers\CustomersController@findCustomer']);

Route::post('/purchase/payment',
    ['as' => 'purchase.payment',
        'uses' => 'Evasquez\PaymentBraintree\Controllers\PurchasesController@store']);

Route::get('/purchase/payment',
    ['as' => 'purchase.payment',
        'uses' => 'Evasquez\PaymentBraintree\Controllers\PurchasesController@store']);

Route::get('/purchase/delete',
    ['as' => 'purchase.delete', 'uses' => function(){
    return Braintree_Customer::delete(Sentry::getUser()->id);
}]);

Route::get('/purchase/paypal', ['as' => 'purchase.delete', 'uses' => function(){
    return Braintree_PaymentMethod::create(array(
        'customerId' => Sentry::getUser()->id,
        'paymentMethodNonce' => 'fake-paypal-one-time-nonce'
    ));
}]);

//Transaction sale
Route::post('/transaction/sale',
    ['as' => 'transaction.sale',
        'uses' => 'Evasquez\PaymentBraintree\Controllers\TransactionsController@sale']);