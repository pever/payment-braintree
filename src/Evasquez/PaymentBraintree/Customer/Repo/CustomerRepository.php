<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\Customer\Repo;

use Braintree_Customer;

class CustomerRepository implements CustomerInterface{

    /**
     * to create customer in payment gateway
     * @param array $attributes
     * @return mixed
     */
    public function createCustomer(array $attributes)
    {
        return Braintree_Customer::create($attributes);
    }

    /**
     * Use Braintree_Customer::find() to find a customer by ID:
     * If the customer cannot be found, you'll receive a Braintree_Exception_NotFound exception.
     * The return value of this call will be a Braintree_Customer response object.
     *
     * https://developers.braintreepayments.com/ios+php/guides/customers
     * @param $user_id
     * @return mixed
     */
    public function findCustomer($user_id)
    {
        return Braintree_Customer::find($user_id);
    }

    /**
     * See also the Braintree::Customer response object.
     * To update a customer, use its ID along with new attributes.
     * The same validations apply as when creating a customer.
     * Any attribute not passed will remain unchanged.
     *
     * @param $user_id
     * @param array $attributes
     * @return mixed
     */
    public function updateCustomer($user_id, array $attributes)
    {
        // TODO: Implement updateCustomer() method.
    }


}