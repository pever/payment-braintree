<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\PaymentMethod\Repo;

use Illuminate\Events\Dispatcher;
use Braintree_PaymentMethod;

/**
 * Class PaymentMethodRepository
 * @package Evasquez\PaymentBraintree\PaymentMethod\Repo
 */
class PaymentMethodRepository implements PaymentMethodInterface
{

    /**
     * @autor eveR Vásquez
     * @link http://evervasquez.me
     * @var Dispatcher
     */
    private $event;

    /**
     * @param Dispatcher $event
     */
    function __construct(Dispatcher $event)
    {
        $this->event = $event;
    }

    /**
     * the paymentMethodNonce[encrypt visa, paypal, etc] parameter
     *
     * @param $customer_id
     * @param array $attributes
     * @return mixed
     */
    public function createPaymentMethod($customer_id, array $attributes)
    {
        $attribs = array_merge(array('customerId' => $customer_id), $attributes);
        return Braintree_PaymentMethod::create($attribs);
    }

    /**
     * If the payment method cannot be found, it will throw a Braintree_Exception_NotFound exception.
     * The return value of the Braintree_PaymentMethod::find() call will be a Braintree_PaymentMethod response object.
     * @param $token
     * @return mixed
     */
    public function findPaymentMethod($token)
    {
        return Braintree_PaymentMethod::find($token);
    }

}