<?php
/*
 *
 *  * Copyright (C) 2015 eveR Vásquez.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

namespace Evasquez\PaymentBraintree\PaymentMethod\Repo;


/**
 * Interface PaymentMethodInterface
 * https://developers.braintreepayments.com/ios+php/guides/payment-methods
 * @package Evasquez\PaymentBraintree\PaymentMethod\Repo
 */
interface PaymentMethodInterface {

    /**
     * the paymentMethodNonce[encrypt visa, paypal, etc] parameter
     *
     * @param $customer_id
     * @param array $attributes
     * @return mixed
     */
    public function createPaymentMethod($customer_id, array $attributes);

    /**
     * If the payment method cannot be found, it will throw a Braintree_Exception_NotFound exception.
     * The return value of the Braintree_PaymentMethod::find() call will be a Braintree_PaymentMethod response object.
     * @param $token
     * @return mixed
     */
    public function findPaymentMethod($token);
}