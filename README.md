

# available Laravel 5..*

## Service Provider
'Evasquez\PaymentBraintree\PaymentBraintreeServiceProvider'

php artisan migrate --package=evasquez/payment-braintree

### workbench
php artisan migrate --bench="evasquez/payment-braintree"


### Published Config
php artisan vendor:publish --tag=payment